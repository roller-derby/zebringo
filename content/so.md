---
title: "Tu es à l’aise sur des patins à roulettes"
date: "2023-03-28"
draft: false
---

Il y a peut-être un poste de *Skating Official* (SO, aussi parfois appelé ref) qui est pour toi. 

Les SO sont reconnaissables à leur maillot rayé blanc et noir. Pour un match officiel, il y en a au moins trois et il peut y en avoir jusqu’à sept ! Dans un crew complet, il y a 4 SO à l’intérieur de la track et 3 SO à l’extérieur. Chaque SO va avoir un point de vue différent sur l’action et sera mieux placé pour repérer un certain type de fautes. 

Tu es peut-être inquiet ou inquiète parce qu’il se passe trop de choses pendant un match et que tu ne va jamais réussir à tout voir : pas de panique ! Chaque SO se concentre seulement sur quelques types de faute. Et même si tu ne siffle aucune faute pendant tes dix premiers matchs, c’est OK, c’est comme ça qu’on apprend. 

![Photo d'un zèbre](../img/zebra.jpg)

- [Tu aimes patiner à l'envers, clique ici.](../oprf/index.html) 
- [Tu préfère patiner en marche avant, c'est ici.](../so_not_oprf/index.html)
- [Après reflexion, t'as un peu la flemme de patiner.](../nso/index.html)

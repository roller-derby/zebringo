---
title: "Track ninja"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste de Track Ninja ! Ce n’est pas un poste d’arbitrage, mais il est tout aussi indispensable au bon déroulement des événements de roller derby que les postes de ref et de NSO. 

Pendant le match, les joueureuses peuvent arracher le scotch qui tient la corde délimitant le bord du track, et il faut quelqu’un pour le remettre en place ! 

Pour accomplir ta mission, tu pourras :

- Arpenter le bord du track pour repérer des défauts. 
- Courir dans tous les sens pour réparer le track entre les jams. 
- Garder un œil ouvert au cas où un ref te fasse signe de venir réparer un bout de track. 
- Suivre le match attentivement afin de voir si les joueureuses décollent le scotch. 
- Te recouvrir le corps de morceaux de scotch prédécoupés. 
- Porter une ceinture à rouleaux de scotch et avoir le swag. 

![Deux tracks ninja qui réparent la track.](../img/track_ninja.jpg)

Il y a aussi plein d'autres postes de bénévole où tu peux te rendre utile. Renseigne-toi auprès de ta ligue !

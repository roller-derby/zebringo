---
title: "Penalty Box Manager"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste de Pénalty Box Manager (PBM) ! 

Ta mission si tu l’acceptes : 

- Chronométrer le temps de pénalité des jammeureuses. 
- Gérer les foul outs. 
- Distribuer des fautes aux joueureuses qui font n’importe quoi en PB (détruire la box, enlever son casque, partir avant la fin du temps de pénalité,…). 
- Aider les PBT s’iels en ont besoin. 
- Si tu es PBM sur un match de Fresh Meat, tu pourras aussi expliquer les règles aux joueureuses qui sont en PB. 

![Photo d'une PB](../img/PBM.png)

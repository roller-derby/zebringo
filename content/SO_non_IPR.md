---
title: "Le pack ? Euh… 10 pieds c’est combien ?"
date: "2023-03-28"
draft: false
---

Une fois que tu auras décidé du poste que tu veux occuper, tu peux choisir un aspect précis de l’arbitrage sur lequel tu veux progresser.

Pendant tes premiers scrimmages, tu peux choisir un ou deux types de pénalités à surveiller. Par exemple « Je ne vais que chercher à voir les Multiplayers. Comme je suis IPR-front je pourrais voir leur impact. », ou bien si tu t’essayes au poste de JR, tu peux commencer par te concentrer sur la prise de lead. 

Avec l’expérience, ça deviendra plus facile de comprendre ce qui se passe dans le jeu et d’avoir un regard plus global. Mais quand tu débutes, c’est OK de te focaliser sur un champ plus restreint. 

Pendant les entrainements aussi tu peux t’entrainer à l’arbitrage. Par exemple, tu peux aussi essayer de te placer à différents endroits, pour voir comment repérer au mieux telle ou telle faute. 

![Une arbitre qui fait le signe d'un back block](../img/back_block.gif)

[Si tu sais compter jusqu'à 4, clique ici.](../jr/index.html)
[Sinon, clique ici](../oprb/index.html)

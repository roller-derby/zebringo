---
title: "T'as la flemme de compter"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste d’Outer Pack Ref (OPR) middle ou back ! Voici la liste de tes responsabilités : 
- Suivre les déplacements du pack. 
- Surveiller les cuts sur la ligne externe du track. 
- Voir les fautes commises entre bloqueureuses. 

Si tu es à OPR-back, tu seras souvent bien placé·e voir qui a initié une action. N’hésite pas à communiquer avec les autres arbitres pour savoir si une faute doit être sifflée. 

Si besoin, des fautes peuvent être sifflées en « late call » entre deux jams. Mais une fois que le jam suivant à commencer, oublie tout ce qui s’est passé avant et concentre toi sur le jam en cours. À ce moment là, c’est trop tard de toutes façons pour revenir sur les décisions qui ont été prises pendant le jam précédent. 

Si le pack se déplace trop vite, les OPR peuvent échanger leurs rôles en cours de jam pour avoir moins de distance à parcourir. Parle-en avec les OPR plus expérimenté·és de ta ligue !

![Photo d'une arbitre qui indique un cut](cut.png)

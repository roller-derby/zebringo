---
title: "Tu adores l'odeur des feutres effaçables"
date: "2023-03-29"
draft: false
---

Bravo ! Tu as gagné le poste de *Penalty Wrangler* (PW) ! Hélas, c’est souvent le premier poste à être supprimé lorsqu’il n’y a pas assez de monde pour un crew d’arbitres complet.

Tu auras pour mission :
- Faire attention aux gestes des arbitres pour savoir quel type de faute est sifflée.
- Communiquer avec les PLT pour les aider à noter les fautes.
- Communiquer avec lae PBM quand iel met une faute. (Oublie pas de regarder de temps en temps vers la PB, au cas où !)

Tu auras à ta disposition un tableau blanc et un feutre pour noter les fautes.

[S'il n'y a pas de PW sur le prochain scrimmage, tu peux cliquer ici pour choisir un autre poste de NSO.](../nso/index.html)

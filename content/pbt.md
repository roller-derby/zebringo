---
title: "Les chronos te font pas peur"
date: "2023-03-29"
draft: false
---

Bravo ! Tu as gagné le poste de *Penalty Box Timer* (PBT) ! 

- Chronométrer les pénalités des bloqueureuses.
- Dire aux bloqueureuses en PB quand se lever et quand retourner en jeu.
- Communiquer avec les PLT pendant les arrêts de jeu pour être sûr d’avoir noté le bon nombre de fautes.

Hésite pas à demander de l’aide à lae PBM si pendant le match tu te sens débordé·e ou si tu as un doute sur ce qu’il faut faire. Iel est souvent plus expérimenté·e et pourra t’aider à gérer.

![](../img/pbt.png)

[Si tu trouves ce poste sympa, mais tu es curieux·se de voir à quoi ressemblent les fautes de plus près, clique ici.](../plt/index.html)

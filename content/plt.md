---
title: "Tu veux être au cœur de l’action"
date: "2023-03-29"
draft: false
---

Bravo ! Tu as gagné le poste de *Penalty Lineup Tracker* (PLT) ! Tu seras toujours débordé.e entre les jams, mais tu pourras profiter du jeu et devenir hyper calé·e sur les différents types de fautes. 

Tu entendras peut-être parler de « Penalty Tracker » et de « Lineup Tracker » (et certains clubs ont encore les anciennes feuilles de marque). En effet, avant il y avait deux postes distincts pour noter les pénalités et les lineups. Mais ça fait maintenant plusieurs années que les deux postes ont fusionné. 

Ta mission :
- Noter le numéro et le poste des joueureuses en début de jam.
- Vérifier qu’il n’y a pas trop de gens qui jouent (oublie pas de regarder qui est en PB).
- Noter chaque pénalité et son type.
- Vérifier avec les PBT que vous avez noté le même nombre de fautes.

![Une PLT qui est en panique parce qu'elle a vu quelqu'un partir en PB mais qu'elle n'a pas entendu la faute.](../img/plt.jpg)

Si tu as de la chance, il y aura un PW qui t’aidera à savoir quelle faute à été sifflée.

[Si tu aimes le concept de ce poste, mais que tu regrettes de ne pas avoir les mains pleines de feutre effaçable à la fin du match, clique ici](../pw/index.html)

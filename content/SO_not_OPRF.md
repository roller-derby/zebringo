---
title: "Les croisés en marche arrière, c'est pas ton kiff"
date: "2023-03-28"
draft: false
---

Quelque soit ton poste, tu vas avoir un certain nombre de responsabilités en tant qu’arbitre. Parmi ces responsabilités, il y a : 

- Assurer la sécurité des joueureuses. 
- Permettre le déroulement du jeu. 
- Pénaliser les avantages obtenus de façon illégale. 
- Faire preuve de neutralité, d’impartialité et d’équité. 
- Enseigner les règles du roller derby aux autres. 

![Un.e NSO qui essaye de rester neutre alors qu'iel a très envie d'encourager les équipes.](../img/neutral_nso.png)

- [Le pack n'a aucun secret pour toi](../ipr/index.html)
- [Eeeuh... 6 pieds c'est combien déjà ?](../so_non_ipr/index.html)

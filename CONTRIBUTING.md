Vous voulez contribuer à Zèbringo ?
- Vous avez une idée de truc à ajouter à l'aventure.
- Vous aimez pas la formulation que j'ai choisi.
- Vous avez repéré une erreur.
- Vous avez une super blague en tête que vous voudriez voir incluse.
- Etc...

Le plus simple, c'est de se rendre sur [la page des tickets](https://framagit.org/Moutmout/zebringo/-/issues) et d'en créer un nouveau dans lequel vous décrivez votre idée.
